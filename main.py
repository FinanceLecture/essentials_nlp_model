# imports
from os import listdir
import random

import librosa
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import warnings

from utils import padding

warnings.filterwarnings('ignore')
import matplotlib as plt
import tensorflow
from tensorflow.keras.layers import LSTM, Dense, Dropout
from sklearn.ensemble import RandomForestClassifier
from keras.utils import to_categorical

# import from files
from feature_analysis import get_waveplot, display_MFCC, get_distribution
from feature_extraction import store_load_features, one_hot_encode
from model_LSTM import LSTM_model
from model_CNN import CNN_model
from evaluation import plot_acc_and_loss, get_confusion_matrix, get_confusion_matrix_rf



path = 'Dataset/audiorecord.webm'


# get_waveplot(path)

# create data by extracting features and targets
features = []  # list to save features
targets = []  # list to save targets
t_start = None
t_end = None
sr = 28000
n_mfcc = 128
# total file names

# length of the frame window/number of samples in windwow
n_fft = 255

# the signal is divided into small frames of 25ms (standard)
# this is done to not loose the contours of the signal over time
# the default spacing between frames (moving window approach), by how much the window is shifted = hop_length
hop_length = 512
# sr=2
# n_mfcc=10
# n_fft=10
# hop_length=12
count = 0



# load the file
y, sr = librosa.load(path, sr=sr)

# cut the file from t_start to t_end in case they are set (not None)
if t_start and t_end:
    print("Audio is cut")
    y = y[round(t_start * sr, ndigits=None)
          :round(t_end * sr, ndigits=None)]

# retrieve the MFCC matrix with frame window n_fft, step size of rolling window: hop_length, and
# number of mfcc returned: n_mfcc.
# add zero padding to matrix so that new dimension is
data = np.array([padding(librosa.feature.mfcc(y, sr=sr,
                                              n_fft=n_fft, hop_length=hop_length, n_mfcc=n_mfcc), 128, 600)])


# collect features
features.append(data)
output = np.concatenate(features, axis=0)

# name the features and targets
X = np.array(output)


path_to_CNN = 'storage/model/CNN_bestModel'
loaded_CNN_model = tensorflow.keras.models.load_model(path_to_CNN)

pred = loaded_CNN_model.predict(X)
prediction_classes = np.argmax(pred, axis=1)


# needs to be deleted
test_path = 'Dataset/test'

# create a list of paths
filenames_test = [f'{test_path}/{name}' for name in listdir(test_path)]
X_test, y_test = store_load_features(filenames_test[500:], 'test', '_with_rest_of_500')
labelencoder = LabelEncoder()
y_train = labelencoder.fit_transform(y_test)
# alternatively use this mapper:
label2language = {
    0: "de",
    1: "en",
    2: "es"
}
# delete until here

# prints(  es or en or de)
print(labelencoder.inverse_transform([prediction_classes])[0])


test = 0
#
#
#
#
#
# # data source
# # https://www.kaggle.com/datasets/toponowicz/spoken-language-identification?resource=download-directory
#
#
# # path to datasets
# train_path = 'Dataset/train'
# test_path = 'Dataset/test'
#
# # create a list of paths
# filenames_test = [f'{test_path}/{name}' for name in listdir(test_path)]
# filenames_train = [f'{train_path}/{name}' for name in listdir(train_path)]
#
# # randomly shuffle the list
# random.Random(4).shuffle(filenames_test)
# random.Random(4).shuffle(filenames_train)
#
#
# ######### PREPROCESSING ############
# # https://towardsdatascience.com/recurrent-neural-nets-for-audio-classification-81cb62327990
# nr_files = len(filenames_test)
#
# # Analysis of the data
# # create and save waveplot of audio
# get_waveplot(filenames_test[0])
# # create and save MFCC
# display_MFCC(filenames_test[0])
#
#
# # retrieve the MFCC numpy arrays for test files
# # if the file with the ending already exists it gets loaded, if not the MFCC is created newly and saved
# X_test, y_test = store_load_features(filenames_test, 'test', '_500_total')
# # retrieve the MFCC numpy arrays for test files
# # if the file with the ending already exists it gets loaded, if not the MFCC is created newly and saved
# X_train, y_train = store_load_features(filenames_train[:5000], 'train', '5000')
#
# # X_test, y_test = store_load_features(filenames_test[500:], 'test', '_with_rest_of_500')
# # X_train, y_train = store_load_features(filenames_test[:500], 'train', 'with_test')
# # X_test, y_test = store_load_features(filenames_test, 'test')
#
#
# # displays and stores the distribution of labels
# get_distribution(y_train, 'train')
#
# # get the shape of the loaded data
# print(f'Shape of X: {X_train.shape}')
# print(f'Shape of y: {y_train.shape}')
#
# # Normalize the data with MinMax Normalization
# # https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html
# X_train = np.array((X_train-np.min(X_train))/(np.max(X_train)-np.min(X_train)))
# X_test = np.array((X_test-np.min(X_test))/(np.max(X_test)-np.min(X_test)))
#
# # split the training set into validation and training
# X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.25, random_state=123)
# print(f'Datasplit was executed')
#
# # one hot encode all target labels
# # https://towardsdatascience.com/categorical-encoding-using-label-encoding-and-one-hot-encoder-911ef77fb5bd
# labelencoder = LabelEncoder()
#
# # get numerical representation
# y_train = labelencoder.fit_transform(y_train)
# y_test = labelencoder.transform(y_test)
# y_val = labelencoder.transform(y_val)
#
# # create one-hot-encoding
# one_hot_train = one_hot_encode(y_train)
# one_hot_val = one_hot_encode(y_val)
# one_hot_test = one_hot_encode(y_test)
#
# print("Preprocessing finished")
#
#
# ###### Start training ########
#
# #  compile the model
# LSTM_model.compile(optimizer='adam'
#                    ,loss='CategoricalCrossentropy'
#                    , metrics=['acc'])
# CNN_model.compile(optimizer='adam',
#               loss=tensorflow.keras.losses.categorical_crossentropy,
#               metrics=['acc'])
#
#
# # set training parameters
# nr_epochs = 40
# nr_epochs_LSTM = 100
# batch_size = 128
#
# # initialize EarlyStopping regularization technique
# callback = tensorflow.keras.callbacks.EarlyStopping(monitor='val_acc', patience=12)
#
#
# # train both models
# history_LSTM = LSTM_model.fit(X_train, one_hot_train, epochs=nr_epochs_LSTM, batch_size=batch_size,
#                     validation_data=(X_val, one_hot_val), shuffle=False)
# history_CNN = CNN_model.fit(X_train, one_hot_train, epochs=nr_epochs,
#                     validation_data=(X_val, one_hot_val), callbacks=[callback])
#
#
#
# ##### Train Benchmark (RandomForest) ##########
# rf = RandomForestClassifier(n_estimators = 1000, random_state = 42, verbose=2)
# X_train_rf = X_train.reshape(len(X_train), 128*600)
# X_test_rf = X_test.reshape(len(X_test), 128*600)
# print(f'Shape of X: {X_train_rf.shape}')
# print(f'Shape of y: {y_train.shape}')
# rf.fit(X_train_rf, y_train)
#
# # Benchmark performance evaluation
# get_confusion_matrix_rf(X_test_rf, y_test, rf)
# #get_confusion_matrix_rf(X_train_rf, y_train, rf)
#
#
# ###### Evaluate the Performance (NN)#########
#
#
# # plot the accuracy and loss over time
# plot_acc_and_loss(history_LSTM, nr_epochs_LSTM, 'LSTM')
# plot_acc_and_loss(history_CNN, len(history_CNN.epoch), 'CNN')
#
#
# # confusion matrix LSTM and CNN on training set
# get_confusion_matrix(target=y_train, X=X_train, model=LSTM_model,
#                      all_labels=list(labelencoder.classes_), type='train_LSTM')
# get_confusion_matrix(target=y_train, X=X_train, model=CNN_model,
#                      all_labels=list(labelencoder.classes_), type='train_CNN')
#
#
# # confusion matrix LSTM and CNN on test set
# get_confusion_matrix(target=y_test, X=X_test, model=LSTM_model,
#                      all_labels=list(labelencoder.classes_), type='test_LSTM')
# get_confusion_matrix(target=y_test, X=X_test, model=CNN_model,
#                      all_labels=list(labelencoder.classes_), type='test_CNN')
#
# print('Evaluation is finished')
#
#
# ##### Save Models ##########
#
# # the model should only be saved if it is better
# # evaluate manually
# print('Should the LSTM model be saved?:')
# inp = input()
#
# # path to model
# path_to_LSTM = 'storage/model/LSTM_bestModel'
# path_to_CNN = 'storage/model/CNN_bestModel'
#
# if inp.startswith('y') or inp.startswith('j'):
#     LSTM_model.save(path_to_LSTM)
#
# print('Should the CNN model be saved?:')
# inp = input()
# if inp.startswith('y') or inp.startswith('j'):
#     CNN_model.save(path_to_CNN)
