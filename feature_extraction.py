import librosa
import numpy as np
import warnings
warnings.filterwarnings('ignore')
import os.path
from tqdm import tqdm
import tensorflow as tf
import tensorflow_io as tfio
import soundfile
from pathlib import PurePath

# import from files
from utils import padding

def extract_features(file_names):
    # create data by extracting features and targets
    features = []  # list to save features
    targets = []  # list to save targets
    t_start = None
    t_end = None
    sr = 28000
    n_mfcc = 128
    # total file names:
    nr_files = len(file_names)

    # length of the frame window/number of samples in windwow
    n_fft = 255

    # the signal is divided into small frames of 25ms (standard)
    # this is done to not loose the contours of the signal over time
    # the default spacing between frames (moving window approach), by how much the window is shifted = hop_length
    hop_length = 512
    # sr=2
    # n_mfcc=10
    # n_fft=10
    # hop_length=12
    count = 0

    for name in tqdm(file_names):

        # retrieve the targets
        target = name.split("/")[-1][:2]
        # save target
        targets.append(target)

        # load the file
        y, sr = librosa.load(name, sr=sr)

        # cut the file from t_start to t_end in case they are set (not None)
        if t_start and t_end:
            print("Audio is cut")
            y = y[round(t_start * sr, ndigits=None)
                  :round(t_end * sr, ndigits=None)]

        # retrieve the MFCC matrix with frame window n_fft, step size of rolling window: hop_length, and
        # number of mfcc returned: n_mfcc.
        # add zero padding to matrix so that new dimension is
        data = np.array([padding(librosa.feature.mfcc(y, sr=sr,
                                                      n_fft=n_fft, hop_length=hop_length, n_mfcc=n_mfcc), 128, 600)])

        # print message only once
        if name.split("/")[-1] == 'de_f_63f5b79c76cf5a1a4bbd1c40f54b166e.fragment1.flac':
            print(data.shape)

        # collect features
        features.append(data)

    # stack the data on top of each other in one array
    output = np.concatenate(features, axis=0)

    # name the features and targets
    X = np.array(output)
    y = np.array(targets)

    return X, y


def store_load_features(filenames, type, specific=""):
    # path and naming configurations
    path_to_feat_test = 'storage/audio_test'
    path_to_feat_train = 'storage/audio_train'
    train_file = 'train'
    test_file = 'test'

    # initialization of parameters
    path_to_feat = None
    file = None
    X = None
    y = None

    # set the right path depending on training or test data
    if type == 'train':
        path_to_feat = path_to_feat_train
        file = train_file
    else:
        path_to_feat = path_to_feat_test
        file = test_file

    # check if test file exists for a certain path
    if not os.path.isfile(f'{path_to_feat}/{file}{specific}.npy'):
        print(f'Saving file: {file}{specific}')
        # create data by extracting features and targets
        X, y = extract_features(filenames)

        # save extracted features
        with open(f'{path_to_feat}/{file}{specific}.npy', 'wb') as f:
            np.save(f, X)
            np.save(f, y)
    else:
        # if file already exist, retrieve information from there
        print(f'Loading file: {file}{specific}')
        with open(f'{path_to_feat}/{file}{specific}.npy', 'rb') as f:
            X = np.load(f)
            y = np.load(f)

    return X, y


def one_hot_encode(y_label):

    # initialize zeros array of correct size
    one_hot = np.zeros((y_label.size, y_label.max() + 1))

    # set ones at the position of label
    one_hot[np.arange(y_label.size), y_label] = 1

    return one_hot


# def load_wav_16k_mono(filename):
#     # Load encoded wav file
#     file_contents = tf.io.read_file(filename)
#     # Decode wav (tensors by channels)
#     wav, sample_rate = tf.audio.decode_wav(file_contents, desired_channels=1)
#     # Removes trailing axis
#     wav = tf.squeeze(wav, axis=-1)
#     sample_rate = tf.cast(sample_rate, dtype=tf.int64)
#     # Goes from 44100Hz to 16000hz - amplitude of the audio signal
#     wav = tfio.audio.resample(wav, rate_in=sample_rate, rate_out=16000)
#     return wav
#
# def preprocess(file_path_flac, label):
#     audio, sr = soundfile.read(file_path_flac)
#     new_wav_path = f'Dataset/wav/{file_path_flac.split("/")[-2]}/{file_path_flac.split("/")[-1][:-5]}.wav'
#     soundfile.write(new_wav_path, audio, sr, 'PCM_16')
#
#     wav = load_wav_16k_mono(new_wav_path)
#     wav = wav[:48000]
#     zero_padding = tf.zeros([48000] - tf.shape(wav), dtype=tf.float32)
#     wav = tf.concat([zero_padding, wav],0)
#     spectrogram = tf.signal.stft(wav, frame_length=320, frame_step=32)
#     spectrogram = tf.abs(spectrogram)
#     spectrogram = tf.expand_dims(spectrogram, axis=2)
#     return spectrogram, label