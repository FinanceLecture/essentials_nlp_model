import matplotlib.pyplot as plt
import numpy as np

DEFAULT_PATH = 'graphs'


def save_image(filename, plt):
    plt.savefig(f'{DEFAULT_PATH}/{filename}')


def padding(array, xx, yy):
    """
    :param array: numpy array
    :param xx: desired height
    :param yy: desirex width
    :return: padded array
    """

    h = array.shape[0]
    w = array.shape[1]

    a = (xx - h) // 2
    aa = xx - a - h

    b = (yy - w) // 2
    bb = yy - b - w

    return np.pad(array, pad_width=((a, aa), (b, bb)), mode='constant')
