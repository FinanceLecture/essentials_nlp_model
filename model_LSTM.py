import tensorflow
from tensorflow.keras.layers import LSTM, Dense, Dropout

input_shape=(128,600)
LSTM_model = tensorflow.keras.Sequential()
LSTM_model.add(LSTM(64,input_shape=input_shape, return_sequences=True))
LSTM_model.add(LSTM(32,input_shape=input_shape))
LSTM_model.add(Dense(64, activation='relu'))
LSTM_model.add(Dropout(0.4))
LSTM_model.add(Dense(32, activation='relu'))
LSTM_model.add(Dropout(0.4))
LSTM_model.add(Dense(3, activation='softmax'))
print(LSTM_model.summary())
