import matplotlib.pyplot as plt
import numpy as np
from sklearn import metrics
from utils import save_image
from sklearn.metrics import confusion_matrix, accuracy_score, plot_confusion_matrix

def plot_acc_and_loss(history, nr_epochs, type):
    # extract the loss and accuracy from history object
    history_dict = history.history
    loss_values = history_dict['loss']
    acc_values = history_dict['acc']
    val_loss_values = history_dict['val_loss']
    val_acc_values = history_dict['val_acc']
    epochs = range(1, 1 + nr_epochs)

    # plot the history (acc and loss) accordingly
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 5))
    ax1.plot(epochs, loss_values, 'co', label='Training Loss')
    ax1.plot(epochs, val_loss_values, 'm', label='Validation Loss')
    ax1.set_title('Training and validation loss')
    ax1.set_xlabel('Epochs')
    ax1.set_ylabel('Loss')
    ax1.legend()

    ax2.plot(epochs, acc_values, 'co', label='Training accuracy')
    ax2.plot(epochs, val_acc_values, 'm', label='Validation accuracy')
    ax2.set_title('Training and validation accuracy')
    ax2.set_xlabel('Epochs')
    ax2.set_ylabel('Accuracy')
    ax2.legend()

    save_image(f'Acc_Loss_{type}.png', plt)
    plt.show()

def get_confusion_matrix(target, X, model, all_labels, type=None):
    # predict labels with model
    pred = model.predict(X)

    # get numerical representation of label (1, 2, 3)
    prediction_classes = np.argmax(pred, axis=1)

    # create and plot confusion matrix
    confusion_matrix = metrics.confusion_matrix(target, prediction_classes)
    cm_display = metrics.ConfusionMatrixDisplay(confusion_matrix = confusion_matrix, display_labels = all_labels)
    cm_display.plot()

    save_image(f'Confusion_matrix_{type}.png', plt)

    plt.show()

def get_confusion_matrix_rf(X, y, cls):
    prediction = cls.predict(X)
    plot_confusion_matrix(cls, X, y)

    save_image(f'Confusion_matrix_RF.png', plt)
    print(f'Random forest on: {accuracy_score(y, prediction)}')

    # plt.show()