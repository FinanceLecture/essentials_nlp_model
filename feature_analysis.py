import librosa
import librosa.display
import matplotlib.pyplot as plt
from utils import save_image
import soundfile as sf
import numpy as np
import seaborn as sns


def get_waveplot(path, sampling_rate=None):

    # y is the audio time series,
    # sr is the sampling rate = number of samples per second (it is measured in samples per Hertz or simply Hz)
    # it is read into the the program as mono sound
    y, sr = librosa.load(f'{path}')

    sr = sampling_rate if sampling_rate else sr

    librosa.display.waveshow(y, sr=sr, x_axis='time', color='purple', offset=0.0)

    # mark y-axis with the measurement unit
    plt.ylabel('Hertz')

    save_image(f'Waveplot_{path.split("/")[-1]}.png', plt)
    plt.show()

def display_MFCC(path):
    # the signal is divided into small frames of 25ms (standard)
    # this is done to not loose the contours of the signal over time
    # the default spacing between frames (moving window approach), by how much the window is shifted = hop_length
    # https://medium.com/@tanveer9812/mfccs-made-easy-7ef383006040#:~:text=25ms%20is%20standard%20.,hop%20length%20of%20160%20samples.
    hop_length = 512

    # length of the frame window/number of samples in windwo
    n_fft = 500

    # load the audio signal, where y is the audio time series,
    y, sr = librosa.load(f'{path}')

    # creates the MFCC matrix (np.array) of shape (,Duration of frames)
    # n_mfcc is the number of mfcc to return
    # choose optimal length length of n_mfcc, sr and n_fft
    MFCCs = librosa.feature.mfcc(y=y, sr=3, n_fft=n_fft, hop_length=hop_length, n_mfcc=10)

    # plot the whole MFCC coeficcients
    fig, ax = plt.subplots()
    librosa.display.specshow(MFCCs,sr=sr, cmap='cool',hop_length=hop_length)
    ax.set_xlabel('Time', fontsize=10)
    # ax.set_title('MFCC', size=20)
    plt.colorbar()

    save_image(f'MFCC_{path.split("/")[-1]}_new.png', plt)


def get_distribution(y, type):

    plt.figure(figsize=(10, 6))
    plot = sns.countplot(y, palette=['#861495',"#e11efb", '#F4A2FF'])
    plt.title("Count of records in each class", size=15)
    plt.xticks(rotation="horizontal", size = 15)
    plot.set_ylabel("Count", fontsize=15)

    save_image(f'occurences_{type}.png', plt)



