# Voice Language Detection

This tool detects the language based on an audio file. Multiple voice processing algorithms are used for preprocessing before a LSTM and a CNN are run on the Mel Frequency Cepstral Coefficients Map to detect the language. This would allow in a further step to use another model to translate to that language.

# Project Outline

This project was conducted in the scope of research at UZH.

