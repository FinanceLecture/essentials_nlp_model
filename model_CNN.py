import tensorflow
from tensorflow.keras.layers import LSTM, Dense, Dropout

CNN_model = tensorflow.keras.Sequential()
CNN_model.add(tensorflow.keras.layers.Conv2D(64, (3, 3), activation='relu', input_shape=(128, 600, 1)))
CNN_model.add(tensorflow.keras.layers.MaxPooling2D((2, 2)))
CNN_model.add(tensorflow.keras.layers.Flatten())
CNN_model.add(tensorflow.keras.layers.Dense(128, activation='relu'))
CNN_model.add(Dense(64, activation='relu'))
CNN_model.add(Dense(64, activation='relu'))
CNN_model.add(Dropout(0.4))
CNN_model.add(Dense(32, activation='relu'))
CNN_model.add(Dense(3, activation='softmax'))
print(CNN_model.summary())